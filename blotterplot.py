#-------------------------------------------------------------------------------
# Name:        BlotterPlot
# Purpose:
#
# Author:      John Bollinger
#
# Created:     13/06/2017
# Copyright:   (c) John Bollinger 2017
# Licence:     MIT
#-------------------------------------------------------------------------------
from __future__ import division
from __future__ import print_function

__appname__ = "Blotter Plot"
__author__  = "John Bollinger"
__version__ = "0.1.0"
__license__ = "MIT"

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

class MarketTiming(object):
    """JB's market timing graphs"""
    def __init__(self, length, format, days, dir, file, name):
        self.plotlength = length
        self.finish = 0
        self.start = 0
        self.x = 0
        self.dates = []
        self.index = []
        self.highs = []
        self.lows = []
        self.timeformat = format
        self.daysBetween = days
        self.directory = dir
        self.filename = file
        self.sheetname = name
    def main(self):
        df = pd.read_excel(self.directory + self.filename, self.sheetname, header=1)
        # setup the x-axis
        self.finish = len(df.index)
        self.start = self.finish - self.plotlength
        self.x = np.arange(self.start, self.finish)
        # get the data we need
        self.dates = df['Date'].iloc[-self.plotlength:]
        self.index = df['Close'].iloc[-self.plotlength:]
        self.highs = df['new highs'].iloc[-self.plotlength:]
        self.lows = -df['new lows'].iloc[-self.plotlength:]
        # create the plotting objects
        f, (a0, a1) = plt.subplots(nrows=2, ncols=1, \
            sharex=True, gridspec_kw = {'height_ratios':[2, 1]})
        # price plot
        a0.plot(self.x, self.index)
        a0.grid(True)
        # indicator plot
        a1.vlines(self.x, 0, self.highs, color='g')
        a1.vlines(self.x, 0, self.lows, color='r')
        a1.grid(True)
        a1.axhline(y=0.0, linewidth=0.4, color='k')
        # get rid of the top indicator tick
        a1.set_yticks(a1.get_yticks()[:-1])
        # eliminate the space between the plots
        plt.subplots_adjust(bottom = 0.15, hspace = 0)
        # format the dates
        plt.xticks(self.x[::self.daysBetween], \
            [date.strftime(self.timeformat) for date in self.dates[::self.daysBetween]], \
            rotation=30, ha ='right')
        plt.show()

if __name__ == '__main__':

# user variables
    length = 250
    format = '%d-%b-%y' # nn-mmm-yy
    days = 21
    dir = 'c:\\users\\john\\my documents\\'
    file = 'blotter2.xlsm'
    sheetname = 'blotter'

# create the object
    timeit = MarketTiming(length, format, days, dir, file, sheetname)
# show the graphs
    timeit.main()

# That's all folks!
