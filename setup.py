from setuptools import setup

setup(name='SystemView',
	version='0.1',
	description='Stock Market Timing Charts.',
	url='https://bitbucket.org/BBands/MarketTiming',
	author='John Bollinger',
	author_email='bbands@gmail.com',
	license='MIT',
	packages=['matplotlib', 'numpy', 'pandas'],
	install_requires=[
		'numpy',
		'matplotlib',
		'pandas'
	],
	data_files=[('sample_data', ['blotter.xlsm'])],
	zip_safe=False)